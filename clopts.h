/******************************************************************************
 * clopts (header) - Command line options processing (header).
 * Sat Mar 16 19:06:30 GMT 2019
 * Copyright (C) 2019-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of clopts.
 *
 * clopts is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * clopts is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with clopts; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2020-04-09 Add convert_int32().
 * 2020-04-09 Add convert_string().
 * 2022-05-21 Add help_text.
 * 2022-05-29 Invalid options.
 * 2022-05-30 Stop processing options.
 * 2022-06-11 Add LGPL licensing.
 * 2023-08-04 Add rudimentary formatting of help text.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __CLOPTS_H__
#define __CLOPTS_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
    char *brief;
    char *full;
    int32_t consume;
    void (*converter)(char *arg, void **data, size_t *size, int32_t *done);
    int32_t done;
    void *data;
    size_t size;
    const char *help_text;
    void (*print_data)(void *data);
}
clopt_t;

void clopts_convert_none(char *arg, void **data, size_t *size, int32_t *done);
void clopts_list_help(char *arg, void **data, size_t *size, int32_t *done);
void clopts_convert_int32(char *arg, void **data, size_t *size, int32_t *done);
void clopts_convert_string(char *arg, void **data, size_t *size, int32_t *done);
void clopts_convert_float(char *arg, void **data, size_t *size, int32_t *done);
void clopts_convert_double(char *arg, void **data, size_t *size, int32_t *done);

int32_t clopts_process(clopt_t *options, size_t noptions, int32_t argc, char *argv[], int32_t start);
int32_t clopts_free(clopt_t *options, size_t noptions);

void clopts_invalid_report_none(int32_t index, char *string);
void clopts_invalid_report_default(int32_t index, char *string);

void clopts_invalid_report_set(void (*invalid_report_func)(int32_t index, char *string));

int32_t clopts_dump(clopt_t *options, size_t noptions);
int32_t clopts_print_set(clopt_t *options, size_t noptions);

void clopts_print_none(void *data);
void clopts_print_int32(void *data);
void clopts_print_string(void *data);
void clopts_print_float(void *data);
void clopts_print_double(void *data);

void clopts_stop_processing(char *arg, void **data, size_t *size, int32_t *done);

int32_t clopts_widths_calculate(clopt_t *options, size_t noptions);

size_t clopts_brief_max_get(void);
size_t clopts_full_max_get(void);
size_t clopts_help_text_max_get(void);

#ifdef __cplusplus
}
#endif

#endif /* __CLOPTS_H__ */

