/******************************************************************************
 * clopts - Command line options processing.
 * Sat Mar 16 19:06:30 GMT 2019
 * Copyright (C) 2019-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of clopts.
 *
 * clopts is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * clopts is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with clopts; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2020-04-09 Add convert_int32().
 * 2020-04-09 Add convert_string().
 * 2022-05-21 Add help_text.
 * 2022-05-29 Invalid options.
 * 2022-05-30 Stop processing options.
 * 2022-06-11 Add LGPL licensing.
 * 2023-08-04 Add rudimentary formatting of help text.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "timber.h"
#include "mal.h"
#include "clopts.h"

#define NO_OF_ELEMENTS(array)   (int32_t)(sizeof(array)/sizeof(array[0]))

static int opt_index;
static clopt_t *options_array;
static size_t options_array_size;
static void (*invalid_report)(int32_t index, char *string) = clopts_invalid_report_default;
static int32_t please_exit;

static size_t brief_max;
static size_t full_max;
static size_t help_text_max;

void clopts_convert_none(char *arg, void **data, size_t *size, int32_t *done)
{
}

void clopts_list_help(char *arg, void **data, size_t *size, int32_t *done)
{
    int32_t index;
    size_t brief_len;
    size_t full_len;
    int32_t extra;

    for (index = 0; index < options_array_size; index++)
    {
        if (NULL == options_array[index].help_text)
        {
        }
        else
        {
            clopts_widths_calculate(options_array, options_array_size);
            brief_len = strlen(options_array[index].brief);
            printf("%s ", options_array[index].brief);
            for (extra = 0; extra < (brief_max - brief_len); extra++)
            {
                printf(" ");
            }

            full_len = strlen(options_array[index].full);
            printf("%s ", options_array[index].full);
            for (extra = 0; extra < (full_max - full_len); extra++)
            {
                printf(" ");
            }

            printf("%s\n", options_array[index].help_text);
        }
    }
}

void clopts_convert_int32(char *arg, void **data, size_t *size, int32_t *done)
{
    int32_t input;

    if (1 == sscanf(arg, "%d", &input))
    {
        if (NULL == (*data = MAL_MALLOC(sizeof(int32_t))))
        {
        }
        else
        {
            *size = sizeof(int32_t);
            *(int32_t *)(*data) = input;
            *done = 1;
        }
    }
    else if (errno != 0)
    {
        perror("sscanf");
    }
    else
    {
    }
}

void clopts_convert_string(char *arg, void **data, size_t *size, int32_t *done)
{
    size_t length = strlen(arg) + 1;

    if (NULL == (*data = MAL_MALLOC(length)))
    {
    }
    else
    {
        *size = length;
        strcpy(*data, arg);
        *done = 1;
    }
}

void clopts_convert_float(char *arg, void **data, size_t *size, int32_t *done)
{
    float input;

    if (1 == sscanf(arg, "%f", &input))
    {
        if (NULL == (*data = MAL_MALLOC(sizeof(float))))
        {
        }
        else
        {
            *size = sizeof(float);
            *(float *)(*data) = input;
            *done = 1;
        }
    }
    else if (errno != 0)
    {
        perror("sscanf");
    }
    else
    {
    }
}

void clopts_convert_double(char *arg, void **data, size_t *size, int32_t *done)
{
    double input;

    if (1 == sscanf(arg, "%lf", &input))
    {
        if (NULL == (*data = MAL_MALLOC(sizeof(double))))
        {
        }
        else
        {
            *size = sizeof(double);
            *(double *)(*data) = input;
            *done = 1;
        }
    }
    else if (errno != 0)
    {
        perror("sscanf");
    }
    else
    {
    }
}

/*
 * returns -1 on failure or the index of the last argv entry read.
 */
int32_t clopts_process(clopt_t *options, size_t noptions, int32_t argc, char *argv[], int32_t start)
{
    int32_t status = -1;
    int32_t i;

    if (start < 1)
    {
    }
    else if (start >= argc)
    {
    }
    else
    {
        options_array = options;
        options_array_size = noptions;
        opt_index = start;
        status = 0;
        please_exit = 0;

        while (opt_index < argc)
        {
            for (i = 0; i < noptions; i++)
            {
#if 0
                printf("argv[%d]: %s\n", start, argv[start]);
                printf("options[%d].brief: %s\n", i, options[i].brief);
#endif
                if (( ! strcmp(argv[opt_index], options[i].brief)) ||
                    ( ! strcmp(argv[opt_index], options[i].full)))
                {
                    if (1 == options[i].consume)
                    {
                        if (++opt_index == argc)
                        {
                            break;
                        }
                    }
                    options[i].converter(argv[opt_index], &options[i].data, &options[i].size, &options[i].done);
                    status = opt_index;
                    break;
                }
            }

            if (0 != please_exit)
            {
                break;
            }

            if (i == noptions)
            {
                /* We didn't find a match. */
                invalid_report(opt_index, argv[opt_index]);
                status = -1;
                break;
            }

            opt_index++;
        }
    }
    return status;
}

int32_t clopts_free(clopt_t *options, size_t noptions)
{
    int32_t nfreed = 0;
    int32_t i;
    
    for (i = 0; i < noptions; i++)
    {
        if (NULL != options[i].data)
        {
            MAL_FREE(options[i].data);
            nfreed++;
        }
    }

    return nfreed;
}

void clopts_invalid_report_none(int32_t index, char *string)
{
}

void clopts_invalid_report_default(int32_t index, char *string)
{
    printf("Invalid option %s at position %d.\n", string, index);
}

void clopts_invalid_report_set(void (*invalid_report_func)(int32_t index, char *string))
{
    invalid_report = invalid_report_func;
}

int32_t clopts_dump(clopt_t *options, size_t noptions)
{
    int32_t nset = 0;
    int32_t i;

    for (i = 0; i < noptions; i++)
    {
        printf("%s %s %d %p %d %p %zd %s\n",
                options[i].brief,
                options[i].full,
                options[i].consume,
                options[i].converter,
                options[i].done,
                options[i].data,
                options[i].size,
                options[i].help_text);
        if (1 == options[i].done)
        {
            nset++;
        }
    }

    return nset;
}

int32_t clopts_print_set(clopt_t *options, size_t noptions)
{
    int32_t nset = 0;
    int32_t i;

    for (i = 0; i < noptions; i++)
    {
        if (0 != options[i].done)
        {
            printf("%s %s ",
                    options[i].brief,
                    options[i].full);
            if (NULL == options[i].data)
            {
            }
            else if (NULL != options[i].print_data)
            {
                options[i].print_data(options[i].data);
            }
            printf("\n");
            nset++;
        }
    }

    return nset;
}

void clopts_print_none(void *data)
{
}

void clopts_print_int32(void *data)
{
    printf("%d", *(int32_t *)data);
}

void clopts_print_string(void *data)
{
    printf("%s", (char *)data);
}

void clopts_print_float(void *data)
{
    printf("%f", *(float *)data);
}

void clopts_print_double(void *data)
{
    printf("%f", *(double *)data);
}

void clopts_stop_processing(char *arg, void **data, size_t *size, int32_t *done)
{
    please_exit = 1;
}

int32_t clopts_widths_calculate(clopt_t *options, size_t noptions)
{
    int32_t retcode = -1;
    int32_t i;

    for (i = 0; i < noptions; i++)
    {
        if (NULL != options[i].brief)
        {
            if (strlen(options[i].brief) > brief_max)
            {
                brief_max = strlen(options[i].brief);
            }
        }
        if (NULL != options[i].full)
        {
            if (strlen(options[i].full) > full_max)
            {
                full_max = strlen(options[i].full);
            }
        }
        if (NULL != options[i].help_text)
        {
            if (strlen(options[i].help_text) > help_text_max)
            {
                help_text_max = strlen(options[i].help_text);
            }
        }
    }

    retcode = 0;

    return retcode;
}

size_t clopts_brief_max_get(void)
{
    return brief_max;
}

size_t clopts_full_max_get(void)
{
    return full_max;
}

size_t clopts_help_text_max_get(void)
{
    return help_text_max;
}

