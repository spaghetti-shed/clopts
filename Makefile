#
# Makefile for clopts.
# Sat Mar 16 19:06:30 GMT 2019
#
# Copyright (C) 2019-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of clopts.
#
# clopts is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# clopts is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with clopts; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# 2019-03-16 Initial creation.
# 2022-05-21 Add clopts_demo target.
# 2022-06-11 Add LGPL licensing.
#

CC=gcc
CFLAGS=-Wall -Werror -O -fPIC
CPPTESTFLAGS=-DTESTING
INCLUDES=-I/usr/local/include
LIBS=-lcutl -ltimber -lmal
SHAREDFLAGS=-shared

INSTALLPFX=/usr/local

all: main test shared demo

demo: clopts_demo

test: clopts_test
	./clopts_test

main: clopts

clopts: clopts.h clopts.c
	$(CC) $(CFLAGS) -c clopts.c

clopts_test: clopts_test.c clopts.c clopts.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o clopts_testing.o -c clopts.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -c clopts_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o clopts_test clopts_test.o clopts_testing.o $(LIBS)

shared: clopts test
	$(CC) $(CFLAGS) $(SHAREDFLAGS) -o libclopts.so clopts.o

clopts_demo: clopts clopts_test clopts_demo.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -c clopts_demo.c
	$(CC) $(CFLAGS) -o clopts_demo clopts_demo.o clopts.o $(LIBS)

demo_run: demo
	./clopts_demo -h -i 100 -f 1.23 -d 1.23e6 -- hello there everyone

install:
	mkdir -p $(INSTALLPFX)/include
	cp clopts.h $(INSTALLPFX)/include
	chmod 444 $(INSTALLPFX)/include/clopts.h
	mkdir -p $(INSTALLPFX)/lib64
	cp libclopts.so $(INSTALLPFX)/lib64
	chmod 555 $(INSTALLPFX)/lib64/libclopts.so

uninstall:
	rm -f $(INSTALLPFX)/lib64/libclopts.so
	rm -f $(INSTALLPFX)/include/clopts.h

clean:
	rm -f *.o
	rm -f clopts_test
	rm -f clopts
	rm -f clopts_demo
	rm -f libclopts.so

