/******************************************************************************
 * clopts_demo - Demo program for clopts command line option processing.
 * Sat May 21 10:55:18 BST 2022
 * Copyright (C) 2022-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of clopts.
 *
 * clopts is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * clopts is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with clopts; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-05-21 Initial creation. Extracted from clopts.c.
 * 2022-05-24 Add conversions.
 * 2022-06-11 Add LGPL licensing.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "timber.h"
#include "mal.h"
#include "clopts.h"

#define NO_OF_ELEMENTS(array)   (int32_t)(sizeof(array)/sizeof(array[0]))

#define NO_OF_OBJECTS   1024

clopt_t options[] =
{
    { "-h", "--help",   0, clopts_list_help,       0, NULL, 0, "Show help info.",          clopts_print_none   },
    { "-i", "--int32",  1, clopts_convert_int32,   0, NULL, 0, "32-bit signed integer.",   clopts_print_int32  },
    { "-s", "--string", 1, clopts_convert_string,  0, NULL, 0, "String.",                  clopts_print_string },
    { "-f", "--float",  1, clopts_convert_float,   0, NULL, 0, "Single-precision float.",  clopts_print_float  },
    { "-d", "--double", 1, clopts_convert_double,  0, NULL, 0, "Double-precision float.",  clopts_print_double },
    { "--", "--",       0, clopts_stop_processing, 0, NULL, 0, "Stop processing options.", clopts_print_none   },
};

int main(int argc, char *argv[])
{
    int32_t i;
    int32_t nfreed;
    int32_t nspare;
    int32_t nset;
    int32_t status;

    MAL_OPEN(NO_OF_OBJECTS);

    for (i = 0; i < argc; i++)
    {
        printf("argv[%d]: %s\n", i, argv[i]);
    }

    status = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    printf("status: %d\n", status);

    nset = clopts_dump(options, NO_OF_ELEMENTS(options));
    printf("nset: %d\n", nset);

    nset = clopts_print_set(options, NO_OF_ELEMENTS(options));
    printf("nset: %d\n", nset);

    if (status < (argc - 1))
    {
        printf("Unprocessed:\n");
        for (i = status + 1; i < argc; i++)
        {
            printf("argv[%d]: %s\n", i, argv[i]);
        }
    }

    nfreed = clopts_free(options, NO_OF_ELEMENTS(options));
    printf("nfreed: %d\n", nfreed);

    nspare = MAL_CLOSE();
    if (0 != nspare)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }

    return 0;
}

