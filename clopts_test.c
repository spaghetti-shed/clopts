/******************************************************************************
 * clopts (test) - Command line options processing (test).
 * Sat Mar 16 19:06:30 GMT 2019
 * Copyright (C) 2019-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of clopts.
 *
 * clopts is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * clopts is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with clopts; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2020-04-09 Add convert_int32().
 * 2020-04-10 Add convert_string().
 * 2022-05-21 Add help_text.
 * 2022-05-29 Invalid options.
 * 2022-05-30 Stop processing options.
 * 2022-06-11 Add LGPL licensing.
 * 2023-08-04 Add rudimentary formatting of help text.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "clopts.h"
#include "cutl.h"
#include "mal.h"

void test_clopts_help_none(void);
void test_clopts_help_one_brief(void);
void test_clopts_help_one_full(void);
void test_clopts_int32_brief(void);
void test_clopts_int32_full(void);
void test_clopts_string_brief(void);
void test_clopts_string_full(void);
void test_clopts_float_brief(void);
void test_clopts_float_full(void);
void test_clopts_double_brief(void);
void test_clopts_double_full(void);
void test_clopts_invalid_brief(void);
void test_clopts_invalid_full(void);
void test_clopts_invalid_arbitrary(void);
void test_clopts_invalid_partial(void);
void test_clopts_stop_partial(void);

void test_clopts_widths_calculate(void);

cutl_test_case_t tests[] =
{
    { "clopts_help_none",         test_clopts_help_none         },
    { "clopts_help_one_brief",    test_clopts_help_one_brief    },
    { "clopts_help_one_full",     test_clopts_help_one_full     },
    { "clopts_int32_brief",       test_clopts_int32_brief       },
    { "clopts_int32_full",        test_clopts_int32_full        },
    { "clopts_string_brief",      test_clopts_string_brief      },
    { "clopts_string_full",       test_clopts_string_full       },
    { "clopts_float_brief",       test_clopts_float_brief       },
    { "clopts_float_full",        test_clopts_float_full        },
    { "clopts_double_brief",      test_clopts_double_brief      },
    { "clopts_double_full",       test_clopts_double_full       },
    { "clopts_invalid_brief",     test_clopts_invalid_brief     },
    { "clopts_invalid_full",      test_clopts_invalid_full      },
    { "clopts_invalid_arbitrary", test_clopts_invalid_arbitrary },
    { "clopts_invalid_partial",   test_clopts_invalid_partial   },
    { "clopts_stop_partial",      test_clopts_stop_partial      },
    { "clopts_widths_calculate",  test_clopts_widths_calculate  },
};

void clopts_test_case_setup(void);
void clopts_test_case_teardown(void);

int main(int argc, char *argv[])
{
    printf("clopts test.\n");

    cutl_set_test_lib("clopts", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(clopts_test_case_setup);
    cutl_set_cutl_test_teardown(clopts_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

#define NO_OF_OBJECTS   1024
void clopts_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
}

void clopts_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();

    if (nspare != 0)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }
}

void test_clopts_help_none(void)
{
    clopt_t options[] =
    {
        { "h", "help", 0, clopts_convert_none, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", retcode, -1);
}

void test_clopts_help_one_brief(void)
{
    clopt_t options[] =
    {
        { "-h", "--help", 0, clopts_convert_none, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-h",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 1, retcode);
}

void test_clopts_help_one_full(void)
{
    clopt_t options[] =
    {
        { "-h", "--help", 0, clopts_convert_none, 0 , NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--help",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 1, retcode);
}

void test_clopts_int32_brief(void)
{
    clopt_t options[] =
    {
        { "-i", "--int32", 1, clopts_convert_int32, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-i",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(int32_t), options[0].size);
    TEST_INT32_EQUAL("data", 123, *(int32_t *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_int32_full(void)
{
    clopt_t options[] =
    {
        { "-i", "--int32", 1, clopts_convert_int32, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--int32",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(int32_t), options[0].size);
    TEST_INT32_EQUAL("data", 123, *(int32_t *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_string_brief(void)
{
    clopt_t options[] =
    {
        { "-s", "--string", 1, clopts_convert_string, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-s",
        "fred",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", strlen("fred") + 1, options[0].size);
    TEST_STR_EQUAL("data", "fred", (char *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_string_full(void)
{
    clopt_t options[] =
    {
        { "-s", "--string", 1, clopts_convert_string, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--string",
        "fred",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", strlen("fred") + 1, options[0].size);
    TEST_STR_EQUAL("data", "fred", (char *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_float_brief(void)
{
    clopt_t options[] =
    {
        { "-f", "--float", 1, clopts_convert_float, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-f",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(float), options[0].size);
    TEST_FLOAT_EQUAL("data", 123.0, *(float *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_float_full(void)
{
    clopt_t options[] =
    {
        { "-f", "--float", 1, clopts_convert_float, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--float",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(float), options[0].size);
    TEST_FLOAT_EQUAL("data", 123.0, *(float *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_double_brief(void)
{
    clopt_t options[] =
    {
        { "-d", "--double", 1, clopts_convert_double, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-d",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(double), options[0].size);
    TEST_DOUBLE_EQUAL("data", 123.0, *(double *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_double_full(void)
{
    clopt_t options[] =
    {
        { "-d", "--double", 1, clopts_convert_double, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--double",
        "123",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 2, retcode);
    TEST_PTR_NOT_NULL("data", options[0].data);
    TEST_SIZE_T_EQUAL("size", sizeof(double), options[0].size);
    TEST_DOUBLE_EQUAL("data", 123.0, *(double *)options[0].data);
    TEST_INT32_EQUAL("done", 1, options[0].done);
    MAL_FREE(options[0].data);
}

void test_clopts_invalid_brief(void)
{
    clopt_t options[] =
    {
        { "-h", "--help", 0, clopts_convert_none, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-x",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    clopts_invalid_report_set(clopts_invalid_report_none);
    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", -1, retcode);
}

void test_clopts_invalid_full(void)
{
    clopt_t options[] =
    {
        { "-h", "--help", 0, clopts_convert_none, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "--xray",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    clopts_invalid_report_set(clopts_invalid_report_none);
    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", -1, retcode);
}

void test_clopts_invalid_arbitrary(void)
{
    clopt_t options[] =
    {
        { "-h", "--help", 0, clopts_convert_none, 0, NULL, 0, NULL, clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "banana",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;

    clopts_invalid_report_set(clopts_invalid_report_none);
    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", -1, retcode);
}

void test_clopts_invalid_partial(void)
{
    clopt_t options[] =
    {
        { "-h", "--help",   0, clopts_convert_none,   0, NULL, 0, "Show help info.",        clopts_print_none },
        { "-i", "--int32",  1, clopts_convert_int32,  0, NULL, 0, "32-bit signed integer.", clopts_print_none },
        { "-s", "--string", 1, clopts_convert_string, 0, NULL, 0, "String.",                clopts_print_none },
        { "-f", "--float",  1, clopts_convert_float,  0, NULL, 0, "Single-precision float.", clopts_print_none },
        { "-d", "--double", 1, clopts_convert_double, 0, NULL, 0, "Double-precision float.", clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-i",
        "100",
        "-f",
        "1.23",
        "-x",
        "-d",
        "1.23e20",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;
    int32_t nfreed;

    clopts_invalid_report_set(clopts_invalid_report_none);
    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    TEST_PTR_NOT_NULL("int32 ptr", options[1].data);
    TEST_SIZE_T_EQUAL("int32 size", sizeof(int32_t), options[1].size);
    TEST_INT32_EQUAL("int32 data", 100, *(int32_t *)options[1].data);

    TEST_PTR_NULL("string ptr", options[2].data);

    TEST_PTR_NOT_NULL("float ptr", options[3].data);
    TEST_SIZE_T_EQUAL("float size", sizeof(float), options[3].size);
    TEST_INT32_EQUAL("float data", 1.23, *(float *)options[3].data);

    TEST_PTR_NULL("double ptr", options[4].data);

    nfreed = clopts_free(options, NO_OF_ELEMENTS(options));
    TEST_INT32_EQUAL("nfreed", 2, nfreed);
}

void test_clopts_stop_partial(void)
{
    clopt_t options[] =
    {
        { "-h", "--help",   0, clopts_convert_none,    0, NULL, 0, "Show help info.",          clopts_print_none },
        { "-i", "--int32",  1, clopts_convert_int32,   0, NULL, 0, "32-bit signed integer.",   clopts_print_none },
        { "-s", "--string", 1, clopts_convert_string,  0, NULL, 0, "String.",                  clopts_print_none },
        { "-f", "--float",  1, clopts_convert_float,   0, NULL, 0, "Single-precision float.",  clopts_print_none },
        { "-d", "--double", 1, clopts_convert_double,  0, NULL, 0, "Double-precision float.",  clopts_print_none },
        { "--", "--",       0, clopts_stop_processing, 0, NULL, 0, "Stop processing options.", clopts_print_none },
    };

    char *argv[] =
    {
        "foo",
        "-i",
        "100",
        "-f",
        "1.23",
        "--",
        "-d",
        "1.23e20",
    };
    int32_t argc = NO_OF_ELEMENTS(argv);
    int32_t retcode;
    int32_t nfreed;

    clopts_invalid_report_set(clopts_invalid_report_none);
    retcode = clopts_process(options, NO_OF_ELEMENTS(options), argc, argv, 1);
    TEST_INT32_EQUAL("retcode", 5, retcode);

    TEST_PTR_NOT_NULL("int32 ptr", options[1].data);
    TEST_SIZE_T_EQUAL("int32 size", sizeof(int32_t), options[1].size);
    TEST_INT32_EQUAL("int32 data", 100, *(int32_t *)options[1].data);

    TEST_PTR_NULL("string ptr", options[2].data);

    TEST_PTR_NOT_NULL("float ptr", options[3].data);
    TEST_SIZE_T_EQUAL("float size", sizeof(float), options[3].size);
    TEST_INT32_EQUAL("float data", 1.23, *(float *)options[3].data);

    TEST_PTR_NULL("double ptr", options[4].data);

    nfreed = clopts_free(options, NO_OF_ELEMENTS(options));
    TEST_INT32_EQUAL("nfreed", 2, nfreed);
}

void test_clopts_widths_calculate(void)
{
    clopt_t options[] =
    {
        { "-h", "--help",   0, clopts_convert_none,    0, NULL, 0, "Show help info.",          clopts_print_none },
        { "-i", "--int32",  1, clopts_convert_int32,   0, NULL, 0, "32-bit signed integer.",   clopts_print_none },
        { "-s", "--string", 1, clopts_convert_string,  0, NULL, 0, "String.",                  clopts_print_none },
        { "-f", "--float",  1, clopts_convert_float,   0, NULL, 0, "Single-precision float.",  clopts_print_none },
        { "-d", "--double", 1, clopts_convert_double,  0, NULL, 0, "Double-precision float.",  clopts_print_none },
        { "--", "--",       0, clopts_stop_processing, 0, NULL, 0, "Stop processing options.", clopts_print_none },
    };

    int32_t retcode;
    int32_t nfreed;

    retcode = clopts_widths_calculate(options, NO_OF_ELEMENTS(options));
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TEST_INT32_EQUAL("brief_max", 2, clopts_brief_max_get());
    TEST_INT32_EQUAL("full_max",  8, clopts_full_max_get());
    TEST_INT32_EQUAL("help_text_max",  24, clopts_help_text_max_get());

    nfreed = clopts_free(options, NO_OF_ELEMENTS(options));
    TEST_INT32_EQUAL("nfreed", 0, nfreed);
}

